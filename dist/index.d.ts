export declare type NextFn = {
    (data?: any): void;
};
export declare type Callback = {
    (result: any, next: NextFn): void;
};
export declare type ReadyFn = {
    (...data: Array<any>): void;
};
export declare class Seoaf {
    private _index;
    private _fnList;
    private _dataList;
    static create(run?: Callback): Seoaf;
    private _next(data?);
    constructor(run?: Callback);
    use(run: Callback): Seoaf;
    end(run?: Callback): Seoaf;
    ready(ready: ReadyFn): Seoaf;
}
