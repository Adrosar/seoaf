"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Seoaf {
    constructor(run) {
        this._index = 0;
        this._fnList = [];
        this._dataList = [];
        if (typeof run === "function") {
            // Dodaję podaną funkcję do listy:
            this._fnList.push(run);
        }
    }
    static create(run) {
        return new Seoaf(run);
    }
    _next(data) {
        this._index++;
        if (typeof data !== "undefined") {
            this._dataList[this._index] = data;
        }
        this._fnList[this._index](this._dataList[this._index], this._next.bind(this));
    }
    use(run) {
        // Dodaję podaną funkcję do listy:
        this._fnList.push(run);
        return this;
    }
    end(run) {
        if (typeof run === "function") {
            // Dodaję podaną funkcję do listy:
            this._fnList.push(run);
        }
        // Uruchamiam rozpatrywanie listy funkcji od pierwszej pozycji `_index[0]`:
        if (this._index === 0) {
            this._fnList[this._index](undefined, this._next.bind(this));
        }
        return this;
    }
    ready(ready) {
        this.use((_, next) => {
            ready((data) => {
                next(data);
            });
        });
        return this;
    }
}
exports.Seoaf = Seoaf;
