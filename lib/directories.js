// Moduły:
const path = require('path');

// Katalog (folder) projektu;
const rootDir = path.resolve(__dirname, '..');

// Foldery:
const dir = {
    root: rootDir,
    source: path.resolve(rootDir, 'source'),
    build: path.resolve(rootDir, 'build'),
    dist: path.resolve(rootDir, 'dist'),
    nm: path.resolve(rootDir, 'node_modules'),
    bc: path.resolve(rootDir, 'bower_components'),
    test: path.resolve(rootDir, 'test')
}

// Eksport:
module.exports = dir;