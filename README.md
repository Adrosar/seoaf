# Seoaf

Serial execution of asynchronous functions _(Szeregowe wykonywanie asynchronicznych funkcji)_

### Opis:

Moduł `seoaf` jest zbudowany na podstawie [ts-startek-kit](https://bitbucket.org/Adrosar/ts-startek-kit). Służy on do **szeregowego** wykonywania asynchronicznych funkcji.

### Instalacja:

```
npm install bitbucket:Adrosar/seoaf#0.1.1
```
lub
```
npm install https://bitbucket.org/Adrosar/seoaf#0.1.1
```

Końcówka `0.1.1` oznacza wersję **modułu npm** jest ona również **tag-iem** w repozytorium.


## Dokumentacja:

### Przykład 1:

```javascript
Seoaf.create((_, next) => {
    setTimeout(() => {
        next("A");
    }, 300);
}).use((data, next) => {
    setTimeout(() => {
        next(data + "B");
    }, 30);
}).end((data, _) => {
    console.log(data);//> AB
});
```

### Przykład 2:

```javascript
const foo = {
    ready_1: function (done) {
        setTimeout(() => {
            done("xoo");
        }, 300);
    },
    ready_2: function (done) {
        setTimeout(() => {
            done("yoo");
        }, 30);
    }
}

Seoaf.create().ready(foo.ready_1).use((data, next) => {
    console.log(data);//> xoo
    next();
}).ready(foo.ready_2).end((data, _) => {
    console.log(data);//> yoo
});
```