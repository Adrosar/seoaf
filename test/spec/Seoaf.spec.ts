import { Seoaf, NextFn } from "../../source/index";

if (typeof Seoaf !== "function") {
    throw new Error();
}

if (typeof Seoaf.create !== "function") {
    throw new Error();
}

const proc = new Seoaf();

if (typeof proc !== "object") {
    throw new Error();
}

if (typeof proc.use !== "function") {
    throw new Error();
}

if (typeof proc.end !== "function") {
    throw new Error();
}

if (typeof proc.ready !== "function") {
    throw new Error();
}

const logs_1: Array<string> = [];

Seoaf.create((_, next: NextFn) => {

    setTimeout(() => {
        logs_1.push("A");
        next("A");
    }, 300);

}).use((data: any, next: NextFn) => {

    setTimeout(() => {
        logs_1.push("B");
        next(data + "B");
    }, 30);

}).end((data: any, _) => {

    logs_1.push(data);

    if (logs_1.join("") !== "ABAB") {
        throw new Error();
    }

});

const logs_2: Array<string> = [];

const foo = {
    ready_1: function (done: Function) {
        setTimeout(() => {
            logs_2.push("X");
            done("x");
        }, 300);
    },
    ready_2: function (done: Function) {
        setTimeout(() => {
            logs_2.push("Y");
            done("y");
        }, 30);
    }
}

Seoaf.create().ready(foo.ready_1).use((data: any, next: NextFn) => {

    logs_2.push("-");

    if (data !== "x") {
        throw new Error();
    }

    next();

}).ready(foo.ready_2).end((data: any, _) => {

    if (data !== "y") {
        throw new Error();
    }

    if (logs_2.join("") !== "X-Y") {
        throw new Error();
    }
});
