
export type NextFn = {
    (data?: any): void;
}

export type Callback = {
    (result: any, next: NextFn): void;
}

export type ReadyFn = {
    (...data: Array<any>): void;
}

export class Seoaf {

    private _index: number = 0;
    private _fnList: Array<Callback> = [];
    private _dataList: Array<any> = [];

    static create(run?: Callback): Seoaf {
        return new Seoaf(run);
    }

    private _next(data?: any): void {
        this._index++;

        if (typeof data !== "undefined") {
            this._dataList[this._index] = data;
        }

        this._fnList[this._index](this._dataList[this._index], this._next.bind(this));
    }

    constructor(run?: Callback) {
        if (typeof run === "function") {
            // Dodaję podaną funkcję do listy:
            this._fnList.push(run);
        }
    }

    public use(run: Callback): Seoaf {
        // Dodaję podaną funkcję do listy:
        this._fnList.push(run);
        return this;
    }

    public end(run?: Callback): Seoaf {
        if (typeof run === "function") {
            // Dodaję podaną funkcję do listy:
            this._fnList.push(run);
        }

        // Uruchamiam rozpatrywanie listy funkcji od pierwszej pozycji `_index[0]`:
        if (this._index === 0) {
            this._fnList[this._index](undefined, this._next.bind(this));
        }

        return this;
    }

    public ready(ready: ReadyFn): Seoaf {

        this.use((_, next: NextFn) => {
            ready((data: any) => {
                next(data);
            });
        });

        return this;
    }
}